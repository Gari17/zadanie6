var apples = 20; // Яблоки, кг
var strawberry = 20; // Клубника - 75 р/кг
var apricot = 20; // Абрикос - 35 р/кг
var flour = 20; // Мука - 10 р/кг
var milk = 20; // Молоко - 15 р/литр
var eggs = 50; // Яйца - 3 р/шт
var applePie = 0;
var strawberryPie = 0; 
var apricotPie = 0;

function ingridient(fl, ml, eg){ 
	if (flour - fl < 0) { 
 		flour += 10; 
 		console.log('Кончилась мука! Купили еще 10 кг'); 
 	}
 	if (milk - ml < 0) { 
 		milk += 10; 
 		console.log('Кончилось молоко! Купили еще 10 литров'); 
 	}
 	if (eggs - eg < 0) {
 		eggs += 10; 
 		console.log('Кончились яйца! Купили еще 10 штук'); 
 	}
}

function CookApplePie() { 
 	if (apples - 3 < 0) { 
 		apples += 10; 
 		console.log('Кончились яблоки! Купили еще 10 кг'); 
 	}
 	ingridient(2, 1, 3);
 	apples = apples - 3;
 	flour = flour - 2;
 	milk = milk - 1;
 	eggs = eggs - 3;
 	++applePie;
 	console.log('Яблочный пирог ' +applePie+ ' готов!');
};

function CookStrawberryPie() {
 	if (strawberry - 5 < 0) {
 		strawberry += 10;
 		console.log('Кончилась клубника! Купили еще 10 кг');
 	}
 	ingridient(1, 2, 4);
 	strawberry = strawberry - 5;
 	flour = flour - 1;
 	milk = milk - 2;
 	eggs = eggs - 4;
 	++strawberryPie;
 	console.log('Клубничный пирог ' +strawberryPie+ ' готов!');
};

function CookApricotPie() {
 	if (apricot - 2 < 0) {
 		apricot += 10;
 		console.log('Кончились абрикосы! Купили еще 10 кг')
 	}
 	ingridient(3, 2, 2);
 	apricot = apricot - 2;
 	flour = flour - 3;
 	milk = milk - 2;
 	eggs = eggs - 2;
 	++apricotPie;
 	console.log('Абрикосовый пирог ' +apricotPie+ ' готов!');
};

function proverka(){
 	for (var i = 0; i < 10; i++){
 		CookApplePie();
 		CookStrawberryPie();
 		CookApricotPie();
 	}
 };

proverka(); 

//2

var n;
var sum = 0;	

function sumToRec (n) {
	if (n >= 1) {
		sum += n;
		return sumToRec (n-1);
		} else {
			console.log(sum);
			return sum;
		};
};

function sumToCycle (n) {
	var sum = 0;

	for (var i = 1; i = n; i++) {
		sum +=n;
		n -= 1; 
	};

	console.log(sum);
	return sum;
};
		
sumToRec(50);
sumToCycle(50);
